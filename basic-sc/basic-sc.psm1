
function cle { Clear-Host }

function .. { Set-Location .. }

function mkfile([string]$filename) {
    if ((Get-ChildItem .\ | Select-Object -expand name) -match $filename) {
        Write-Output "A file called $filename already exists."
    }
    else {
        $null > $filename
        Write-Output "File created:  $filename"
    }
}

function mkfolder([string]$folderName) {
    New-Item -Path . -Name $folderName -ItemType "directory"        
}

function lsname() {
    Get-ChildItem | Sort-Object Name
}

function lsdate() {
    Get-ChildItem | Sort-Object LastWriteTime -Descending
}

function functions() {
    Get-ChildItem function:\
}

function aliases() {
    Get-Alias
}