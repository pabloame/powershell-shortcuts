param(
    [parameter(Position = 0, Mandatory = $true)][string]$javaInstalationPath
)

function javav { java -version }

function jdk([string]$version) {
    $toUpdate = (Get-ChildItem $javaInstalationPath | Select-Object -expand name) -match $version;
    if ($toUpdate -and $toUpdate.count -eq 1) {
        Write-Host "JDK version picked is: $toUpdate" -ForegroundColor Green
        Update-JRE("$javaInstalationPath\$toUpdate\bin");
    }
    elseif ($toUpdate -and $toUpdate.count -gt 1) {
        Write-Host "Unable to find a jdk that matches properly: $version" -ForegroundColor Yellow
        Write-Host "Try a more specific query: ";   
        $toUpdate | ForEach-Object {
            Write-Host  " * $($_)";
        }
        Write-Host "";       
    }
    else {
        Write-Host "Unable to find a jdk that matches: $version" -ForegroundColor Red
        Write-Host "Try something like: ";
        Show-Java-Versions($False)
    }
    
}

function Show-Java-Versions([boolean] $printCurrent) {
    Write-Host "";   
    Get-ChildItem $javaInstalationPath | Select-Object -expand name | ForEach-Object {
        $message = " * $($_)";
        if ($printCurrent -and (Get-Content Env:JRE_HOME) -match $_) {
            Write-Host $message  -ForegroundColor Green -NoNewline
            Write-Host " <-- Current"  
        }
        else {
            Write-Host  $message
        }
    }
    Write-Host "";   
}
function javas {
    Write-Host "";   
    Write-Host "You have installed $((Get-ChildItem $javaInstalationPath | Select-Object -expand name).count) java versions: ";
    Show-Java-Versions($True)
}
 
function jshell {
    if ((Get-ChildItem (Get-Content Env:JRE_HOME) | Select-Object -expand name) -match "jshell.exe") {
        Invoke-Expression ((Get-ChildItem (Get-Content Env:JRE_HOME) | Select-Object -expand fullName) -match "jshell.exe")[0]
    }
    else {
        Write-Host "Your current version of java ($(Get-Content Env:JRE_HOME)) does not support jshell, relaying on latest one." -ForegroundColor Red
        $willUse = ((Get-ChildItem "C:\dev\java" | Select-Object -expand name) | Sort-Object -Descending)[0];
        Invoke-Expression "$javaInstalationPath\$($willUse)\bin\jshell.exe"
    }
}


function Update-JRE([string]$path) {
    [System.Environment]::SetEnvironmentVariable("JRE_HOME", $path, "Machine")
    Write-Output "Boot a new shell to get this change."
}
