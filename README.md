# Powershell ShortCuts 

This repository holds some modules that allow me have some shortcuts for repetitive powershell related tasks.


# Installation

Copy and paste each module folder you want to install into Powershell modules folder, usually:

C:\Users\username\Documents\WindowsPowerShell\Modules

And include them as follow in your profile (C:\Users\username\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1):

* Import-Module docker-sc
* Import-Module basic-sc
* Import-Module java-sc


# Interesting modules to import from external repositories

* posh-docker
* DockerCompletion 

