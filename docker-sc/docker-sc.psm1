Set-Alias d docker


function dps {d ps --all}
function di {d images}

function dex([string]$dockername) {
  Write-Host "Changing to /bash in $dockername"
  d exec -it $dockername /bin/bash
}

function dsr([string]$dockername) {
  Write-Host "Stopping and removing $dockername"
  d stop $dockername; d rm $dockername
}

function dsrall() {
  Write-Host "Stopping and removing all containers"
  d stop $(d ps -a -q); d rm $(d ps -a -q);
}

function drstop() {
  Write-Host "Removing stopped containers"
  d rm $(d ps --all -q -f status=exited);
}

function dsall() {
  Write-Host "Stopping all containers"
  d stop $(d ps -a -q);
}

function drall() {
  Write-Output "Removing all containers"
  d rm $(d ps -a -q);
}

function dri() {
  Write-Output "Removing all dangling images"
  d rmi $( d images -q -f dangling=true)
}
